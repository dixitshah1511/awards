-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2018 at 12:27 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `Usr_Id` int(11) NOT NULL,
  `Usr_Name` varchar(255) NOT NULL,
  `Usr_Password` varchar(255) NOT NULL,
  `Usr_Uname` varchar(255) NOT NULL,
  `Usr_Email` varchar(255) NOT NULL,
  `User_profile_img` varchar(255) NOT NULL,
  `User_designation` varchar(255) NOT NULL,
  `User_linkdinprofile` varchar(255) NOT NULL,
  `Usr_Access` varchar(255) NOT NULL,
  `Usr_Status` int(11) NOT NULL,
  `Usr_CreatedOn` date NOT NULL,
  `Usr_CreatedBy` varchar(255) NOT NULL,
  `Usr_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`Usr_Id`, `Usr_Name`, `Usr_Password`, `Usr_Uname`, `Usr_Email`, `User_profile_img`, `User_designation`, `User_linkdinprofile`, `Usr_Access`, `Usr_Status`, `Usr_CreatedOn`, `Usr_CreatedBy`, `Usr_time`) VALUES
(1, 'dixit', '65e4bd4ae706d426942696400c3da58a3f795697', 'dixit shah', 'dixitshah29@yahoo.in', '/userfiles/images/1.jpg', 'Web Executive Developer', '', 'a,b,c', 1, '0000-00-00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `website_settings`
--

CREATE TABLE `website_settings` (
  `Ws_Id` int(11) NOT NULL,
  `Ws_Name` varchar(500) NOT NULL,
  `Ws_Title` varchar(500) NOT NULL,
  `Ws_Email` varchar(500) NOT NULL,
  `Ws_Phone` varchar(500) NOT NULL,
  `Ws_Mobile` varchar(50) NOT NULL,
  `Ws_GMap` varchar(500) NOT NULL,
  `Ws_Logo` varchar(500) NOT NULL,
  `Ws_Url` varchar(500) NOT NULL,
  `Ws_Address` varchar(500) NOT NULL,
  `Ws_Copyright` varchar(500) NOT NULL,
  `Ws_Facebook` varchar(500) NOT NULL DEFAULT '#',
  `Ws_Twitter` varchar(1000) NOT NULL,
  `Ws_LinkedIn` mediumtext NOT NULL,
  `Ws_GooglePlus` varchar(500) NOT NULL,
  `Ws_Instagram` varchar(500) DEFAULT NULL,
  `Ws_Youtube` varchar(500) NOT NULL,
  `Ws_Des` varchar(500) NOT NULL,
  `Ws_Key` varchar(1000) NOT NULL,
  `Ws_SiteMap` varchar(500) DEFAULT NULL,
  `Ws_Contact_Form_Email` varchar(500) DEFAULT NULL,
  `Ws_Google_Web` varchar(500) DEFAULT NULL,
  `Ws_Analytics` varchar(500) DEFAULT NULL,
  `Ws_Google_TM` varchar(500) DEFAULT NULL,
  `Ws_Zopim` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `website_settings`
--

INSERT INTO `website_settings` (`Ws_Id`, `Ws_Name`, `Ws_Title`, `Ws_Email`, `Ws_Phone`, `Ws_Mobile`, `Ws_GMap`, `Ws_Logo`, `Ws_Url`, `Ws_Address`, `Ws_Copyright`, `Ws_Facebook`, `Ws_Twitter`, `Ws_LinkedIn`, `Ws_GooglePlus`, `Ws_Instagram`, `Ws_Youtube`, `Ws_Des`, `Ws_Key`, `Ws_SiteMap`, `Ws_Contact_Form_Email`, `Ws_Google_Web`, `Ws_Analytics`, `Ws_Google_TM`, `Ws_Zopim`) VALUES
(1, 'scientimed', 'scientimed solution', '', '', '', '', '', '', '', '', '', '', '', '', '#', '', '', '', '', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`Usr_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_master`
--
ALTER TABLE `user_master`
  MODIFY `Usr_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
