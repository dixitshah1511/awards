<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
// header('Access-Control-Allow-Origin: *');  
class Admin extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->model('home_model');
    }

	public function index()
	{
		$this->load->view('admin/login');
	}
	
	public function dashboard()
	{
		$this->load->view('admin/dashboard');
	}

	public function showuser(){
	
	$this->load->view('admin/showuser');	
		
	}
	public function adduser()
	{
		$this->load->view('admin/adduser');
	}
	
	public function regist_action()
	{
				$ans=$this->input->post();
		
		$this->form_validation->set_rules('user_name','User Name','required|is_unique[user_register.user_name]');

		$this->form_validation->set_rules('user_mobile','User Mobile','required|regex_match[/^[0-9]{10}$/]|is_unique[user_register.user_mobile]');
		
		$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[user_register.user_email]');

		$this->form_validation->set_rules('user_pass', 'Password', 'required|min_length[6]|max_length[12]|matches[user_cpass]');
		$this->form_validation->set_rules('user_cpass', 'Password Confirmation', 'required');
		
		

		$this->form_validation->set_rules('user_status','User Status','required');

			if($this->form_validation->run()==FALSE)
				{
					echo "<div style='color: #e61212;font-weight: 700;'> ".validation_errors()."</div>";
				}	
			else
			{
				$ans['user_pass']=do_hash($ans['user_pass']);
				unset($ans['user_cpass']);

				$this->myclass->insert('user_register',$ans);
				echo 1;

			}
	}

	public function delete_user($id){
		// print_r($id);
		$this->myclass->delete_data("user_register","user_id='$id'");
		redirect('admin/showuser');

	}
	public function edit_user($id){
		// print_r($id);
		$ans=$this->myclass->select_data("user_id,user_name,user_mobile,user_email,user_pass,user_status","user_register","user_id='$id'");
		// print_r($ans);
		$this->load->view('admin/edit_user',array("result"=>$ans));
	}
	public function update_user(){
		$ans=$this->input->post();
		// print_r($ans);
		$id=$ans['user_id'];
		$ans['user_pass']=do_hash($ans['user_pass']);
		$option=array(

			'user_name'=>$ans['user_name'],
			'user_mobile'=>$ans['user_mobile'],
			'user_email'=>$ans['user_email'],
			'user_pass'=>$ans['user_pass'],
			'user_status'=>$ans['user_status']
		);

		$this->myclass->update_data("user_register",$option,"user_id='$id'");

		// redirect('Admin/edit_user/$id');
		redirect("Admin/edit_user/$id");
	}
	public function admin_loginaction(){
		// echo "1111";
		$answer1 = $this->input->post();
		// print_r($answer1);
		// die();
			$this->form_validation->set_rules('user_email','User Name Is Required ','required');
			$this->form_validation->set_rules('user_pass', 'Password', 'required|alpha_numeric|min_length[6]|max_length[12]');

				if($this->form_validation->run()==FALSE)
				{
					echo validation_errors();
				}		
				else
				{
					$this->load->model("home_model");
					
			   		$ans1=$this->home_model->checkrecord($answer1['user_email'],do_hash($answer1['user_pass']));
			   		// print_r($ans1);
			   		// exit();
			   		 
				   	if($ans1 == 0)
					{
						echo "Invalid Data";
					}
					else
					{
						// print_r($ans1[0]->User_profile_img);
						// exit();
						$this->session->set_userdata('user_id',$ans1[0]->user_id);
						$this->session->set_userdata('user_name',$ans1[0]->user_name);
						$this->session->set_userdata('user_mobile',$ans1[0]->user_mobile);
						$this->session->set_userdata('user_email',$ans1[0]->user_email);
						$this->session->set_userdata('user_status',$ans1[0]->user_status);
					
							
						echo 1;
					}
				}
	}
	
	public function logout(){
		$this->session->unset_userdata('user_id',$ans1[0]->user_id);
		$this->session->unset_userdata('user_name',$ans1[0]->user_name);
		$this->session->unset_userdata('user_mobile',$ans1[0]->user_mobile);
		$this->session->unset_userdata('user_email',$ans1[0]->user_email);
		$this->session->unset_userdata('user_status',$ans1[0]->user_status);
		redirect('Admin/');
	}
	public function viewallproduct(){
		$this->load->view('admin/viewallproduct');
	}
	public function addproduct(){
		$this->load->view('admin/addproduct');	
	}

	public function product_addaction(){
		// echo "111";
		$ans=$this->input->post();
		
		$this->form_validation->set_rules('product_img','Product Image ','required');
		$this->form_validation->set_rules('product_link','Product Link ','required');
		$this->form_validation->set_rules('product_name', 'Product Name', 'required');
		$this->form_validation->set_rules('product_sequence', 'Product Sequence', 'required|is_unique[product.product_sequence]');

				if($this->form_validation->run()==FALSE)
				{
					echo "<div style='color: #e61212;font-weight: 700;'> ".validation_errors()."</div>";
				}	
				else
				{
					// print_r($ans);
					$this->myclass->insert("product",$ans);
					// redirect('Admin/addproduct');
					echo "1";
				}

		// $this->load->view('product_add');
	}
	public function delete_product($id){
		// print_r($id);

		$this->myclass->delete_data("product","product_id='$id'");
		redirect('/admin/viewallproduct');	
	}

	public function enquiery(){
		$this->load->view('admin/enquiery');
	}
	public function edit_product($id){
		// print_r($id);
		$ans=$this->myclass->select_data("product_id,product_img,product_link,product_name,product_sequence","product","product_id='$id'");
		// print_r($ans);
		$this->load->view("admin/edit_product",array("result"=>$ans));
	}
	public function update_action(){
		$ans=$this->input->post();
		// print_r($ans);
		$id=$ans['product_id'];
		$option=array(
			"product_img"=>$ans['product_img'],
			"product_link"=>$ans['product_link'],
			"product_name"=>$ans['product_name'],
			"product_sequence"=>$ans['product_sequence']



			);
		$this->myclass->update_data("product",$option,"product_id='$id'");
		redirect("Admin/edit_product/".$id);
	}
}

 