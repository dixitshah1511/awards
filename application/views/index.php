<html>
	<head>
		<link rel="stylesheet"  href="<?php echo base_url();?>assets/layout/library/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet"  href="<?php echo base_url();?>assets/layout/css/style.css">
		<script src="<?php echo base_url();?>assets/admin/assets/js/jquery-1.11.3.min.js"></script>
		<script src="<?php echo base_url();?>assets/layout/library/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/layout/css/rating.js"></script>
		<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
			<script src="<?php echo base_url();?>assets/admin/js/project.js" ></script>
	</head>
	<body style="background-color:#090909;">
	  <div>
	      <img src="http://fortunestreets.com/awards/assets/layout/images/banner_4-1.jpeg" width="100%">
	   </div>
	   <div class="col-md-2 col-xs-12" style="text-align:right;">
	       
	        
	    </div>
	    <div class="col-md-2 col-xs-12 vt" style="text-align:center;padding-bottom:10px;">
	        <a href="<?php echo base_url();?>Home/vote" class="btn btvote"> VOTE</a>
	        
	    </div>
	    <div class="col-md-2 col-xs-12 re" style="text-align:center;padding-bottom:10px;">
	        <input type="button" value="REGISTER" class="btn btvote" id="btn_product" data-toggle="modal" data-target="#myModal">
	    </div>
	    <div class="col-md-2 col-xs-12 nom" style="text-align:center;">
	        <input type="button" value="NOMINEE" class="btn btvote" id="btn_product" data-toggle="modal" data-target="#myModal">
	        
	    </div>
	    <div class="col-md-2 col-xs-12" style="text-align:right;">
	       
	        
	    </div>
	    
	    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel"></h4>
			      </div>
			      <div class="modal-body">
			        <form id="product_selected" type="post">
			        	<div id="err1" style="color: #2c6923;font-weight: 700;"> </div>
			        	<input type="hidden" id="data_selected" name="product_id">
					    <div class="form-group">
					    <label for="exampleInputEmail1">Name</label>
					    <input type="text" class="form-control" name="product_selectedname" placeholder="Name">
					  	</div>
					   <div class="form-group">
					    <label for="exampleInputEmail1">Email</label>
					    <input type="email" class="form-control" name="product_selectedemail" placeholder="Email">
					  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail1">Phone</label>
					    <input type="text" class="form-control" name="product_selectedphone" placeholder="Phone">
					  </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" id="product_submitted">Submit</button>

			      </div>
			    </div>
			  </div>
			</div>
	</body>
	</html>	