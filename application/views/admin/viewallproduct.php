<?php 
$this->load->view('admin/header');

 ?>
 <div class="col-sm-12">
    <a  class="btn btn-green btn-sm btn-icon icon-left"  href="<?php echo base_url(); ?>Admin/addproduct"> <i class="fa fa-plus-circle"></i>Add New Product</a>
    <bR><bR>
 </div>
<br><br>
 			<div class="well well-sm">
                <h4>View All Admin User</h4>
            </div>
              <table class="table table-bordered datatable" id="table-1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Person Name</th>
                        <th>Person Image</th>
                        <th>Person Sequence</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                        $ans=$this->myclass->select_data("product_id,product_img,product_name,product_sequence","product","1");
                        // print_r($ans);
                        $i=0;
                        if(is_array($ans)){
                        foreach ($ans as  $value) {
                        $i++;

                     ?> 
                     <tr>
                         <td><?php echo $i; ?></td>   
                         <td><?php echo $value->product_name; ?></td>
                         <td><?php echo $value->product_sequence; ?></td>
                       	<td><img src="<?php echo base_url(); ?><?php echo $value->product_img ; ?>" width="100px" height="100px"></td>
                         <td> <a href="<?php echo base_url(); ?>Admin/edit_product/<?php echo $value->product_id; ?>" class="btn btn-primary btn-small">Edit</a> <a href="<?php echo base_url(); ?>Admin/delete_product/<?php echo $value->product_id; ?>" class="btn btn-danger btn-small">Delete</a></td>
                     </tr>
                     <?php }} ?>               
                </tbody>
                <tfoot> 
                	<tr>  
                       <th>ID</th>
                        <th>Person Name</th>
                        <th>Person Image</th>
                        <th>Person Sequence</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table> <br />

 <?php 
$this->load->view('admin/footer');

 ?>