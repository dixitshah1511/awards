<?php 
$this->load->view("admin/header");

 ?>
  <div class="col-sm-12">
    <a  class="btn btn-green btn-sm btn-icon icon-left"  href="<?php echo base_url(); ?>Admin/adduser"> <i class="fa fa-plus-circle"></i>Add New User</a>
    <bR><bR>
 </div>

 			<div class="well well-sm">
                <h4>View All Admin User</h4>
            </div>
              <table class="table table-bordered datatable" id="table-1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User Name</th>
                        <th>User Profile Name</th>
                        <th>User EmailID</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                        $ans=$this->myclass->select_data("user_id,user_name,user_mobile,user_email","user_register","1");
                        // print_r($ans);
                        $i=0;
                        if(is_array($ans)){
                        foreach ($ans as  $value) {
                        $i++;

                     ?> 
                     <tr>
                         <td><?php echo $i; ?></td>   
                         <td><?php echo $value->user_name; ?></td>
                         <td><?php echo $value->user_mobile; ?></td>
                         <td><?php echo $value->user_email; ?></td>
                         <td><a href="<?php echo base_url(); ?>Admin/edit_user/<?php echo $value->user_id; ?>" class="btn btn-primary btn-small">Edit</a> <a href="<?php echo base_url(); ?>Admin/delete_user/<?php echo $value->user_id; ?>" class="btn btn-danger btn-small">Delete</a></td>
                     </tr>
                     <?php }} ?>               
                </tbody>
                <tfoot> 
                	<tr>  
                        <th>ID</th>
                        <th>User Name</th>
                        <th>User Profile Name</th>
                        <th>User EmailID</th>
   
                        <!-- <th>User Access</th> -->
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table> <br />
       	  
            

 <?php 
$this->load->view("admin/footer");

 ?>