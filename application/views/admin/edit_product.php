<?php 
$this->load->view('admin/header');
// print_r($result);
foreach ($result as $value) {
	# code...
	$product_id=$value->product_id;
	$product_img=$value->product_img;
    $product_link=$value->product_link;
	$product_name=$value->product_name;
	$product_sequence=$value->product_sequence;
}
 ?>

<form action="<?php echo  base_url();?>admin/update_action" method="post">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        Product Details
                    </div>
                    <div class="panel-options"> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> </div>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal form-groups-bordered">
                    	<input type="hidden"  name="product_id"  value="<?php echo $product_id; ?>">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Product Image *</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <input type="text" id="Banner" name="product_img" placeholder="" class="form-control" value="<?php echo $product_img; ?>">

                                </div>
                                <div class="">
                                    <input type="button" value="Browse Server" onclick="BrowseServer( 'Images:/', 'Banner' );">
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Person Link *</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <input type="text" name="product_link" placeholder="" value="<?php echo $product_link; ?>" class="form-control">

                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Product Name *</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <input type="text" name="product_name" placeholder="" class="form-control" value="<?php echo $product_name; ?>">

                                </div>
                                
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Product Sequence *</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="product_sequence" value="<?php echo $product_sequence; ?>"></input>
                            </div>
                        </div>

                        


                        <div class="text-center">
                        	<div id="err1" style='color: #2c6923;font-weight: 700;'></div>
                        </div>
                        <div class="modal-footer">

                            <input type="submit" class="btn btn-info" value="Update">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

 <?php 
$this->load->view('admin/footer');
 ?>