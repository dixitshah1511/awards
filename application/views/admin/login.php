
<!DOCTYPE html>
<html lang="en">

<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="Laborator.co" />
    <link rel="icon" href="<?php echo base_url();?>assets/admin/assets/images/favicon.ico">
    <title>CMC | Login</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/font-icons/entypo/css/entypo.css" id="style-resource-2">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/bootstrap.css" id="style-resource-4">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/neon-core.css" id="style-resource-5">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/neon-theme.css" id="style-resource-6">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/neon-forms.css" id="style-resource-7">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/custom.css" id="style-resource-8">
    <script src="<?php echo base_url();?>assets/admin/assets/js/jquery-1.11.3.min.js"></script>
   
</head>

<body class="page-body login-page login-form-fall" >
    <!-- TS151486588719786: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates -->
    <script type="text/javascript">
        var baseurl = '../../index.html';
    </script>
    <div class="login-container">
        <div class="login-header login-caret">
            <div class="login-content"> <a href="../../dashboard/main/index.html" class="logo"> <h2 style="color:#fff;text-transform: uppercase;">CMC</h2> </a>
                <p class="description">Dear user, log in to access the admin area!</p>

            </div>
        </div>
        <div class="login-progressbar">
            <div></div>
        </div>
        <div class="login-form">
            <div class="login-content">
                <div class="form-login-error">
                    <h3>Invalid login</h3>
                   
                </div>
                <form id="loginadmin_form" type="post" >
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                 <i class="entypo-user"></i> 
                             </div>
                              <input type="text" class="form-control" name="user_email"  placeholder="User Email"  /> </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"> 
                                <i class="entypo-key"></i> 
                            </div> 
                            <input type="password" class="form-control" name="user_pass"  placeholder="Password"  /> 
                        </div>
                    </div>
                    <div class="form-group"> 
                        <button type="button" class="btn btn-primary btn-block btn-login" id="btn_login"> <i class="entypo-login"></i>Login In</button>
                    
                    </div>
                    <!-- <div class="form-group"> <em>- or -</em> </div> -->
                    <!-- <div class="form-group"> <button type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left facebook-button">Login with Facebook<i class="entypo-facebook"></i> </button> </div> -->
                </form>
                
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/admin/assets/js/gsap/TweenMax.min.js" id="script-resource-1"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/bootstrap.js" id="script-resource-3"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/joinable.js" id="script-resource-4"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/resizeable.js" id="script-resource-5"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/neon-api.js" id="script-resource-6"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/cookies.min.js" id="script-resource-7"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/jquery.validate.min.js" id="script-resource-8"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/neon-login.js" id="script-resource-9"></script>
    
     <script src="<?php echo base_url();?>assets/admin/js/project.js" ></script>
   
</body>
<!-- Mirrored from demo.neontheme.com/extra/login/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 02 Jan 2018 04:08:28 GMT -->

</html>

