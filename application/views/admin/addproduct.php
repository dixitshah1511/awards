<?php 
$this->load->view('admin/header');
 ?>

<form id="product_form" type="post">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        User Details
                    </div>
                    <div class="panel-options"> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> </div>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal form-groups-bordered">

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Person Image *</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <input type="text" id="Banner" name="product_img" placeholder="" class="form-control">

                                </div>
                                <div class="">
                                    <input type="button" value="Browse Server" onclick="BrowseServer( 'Images:/', 'Banner' );">
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Person Link *</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <input type="text" name="product_link" placeholder="" class="form-control">

                                </div>
                                
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Person Name *</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <input type="text" name="product_name" placeholder="" class="form-control">

                                </div>
                                
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Person Sequence *</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="product_sequence"></input>
                            </div>
                        </div>

                        


                        <div class="text-center">
                        	<div id="err1" style='color: #2c6923;font-weight: 700;'></div>
                        </div>
                        <div class="modal-footer">

                            <input type="button" class="btn btn-info" id="btn_addproduct" value="Add">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

 <?php 
$this->load->view('admin/footer');
 ?>