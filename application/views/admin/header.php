<?php 



 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="Laborator.co" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


    <link rel="icon" href="<?php echo base_url();?>assets/admin/assets/images/favicon.ico">
    <title>CMS</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/font-icons/entypo/css/entypo.css" id="style-resource-2">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/bootstrap.css" id="style-resource-4">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/neon-core.css" id="style-resource-5">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/neon-theme.css" id="style-resource-6">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/neon-forms.css" id="style-resource-7">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/custom.css" id="style-resource-8">

    <script src="<?php echo base_url();?>assets/admin/assets/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo base_url() ?>ckfinder/ckfinder.js"></script>
    <script src=" <?php echo base_url() ?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">

function BrowseServer( startupPath, functionData )
{
   // You can use the "CKFinder" class to render CKFinder in a page:
   var finder = new CKFinder();

   // The path for the installation of CKFinder (default = "/ckfinder/").
   finder.basePath = '../ckfinder';

   //Startup path in a form: "Type:/path/to/directory/"
   finder.startupPath = startupPath;

   // Name of a function which is called when a file is selected in CKFinder.
   finder.selectActionFunction = SetFileField;

   // Additional data to be passed to the selectActionFunction in a second argument.
   // We'll use this feature to pass the Id of a field that will be updated.
   finder.selectActionData = functionData;

   // Name of a function which is called when a thumbnail is selected in CKFinder.
   finder.selectThumbnailActionFunction = ShowThumbnails;

   // Launch CKFinder
   finder.popup();
}

// This is a sample function which is called when a file is selected in CKFinder.
function SetFileField( fileUrl, data )
{
   document.getElementById( data["selectActionData"] ).value = fileUrl;
}

// This is a sample function which is called when a thumbnail is selected in CKFinder.
function ShowThumbnails( fileUrl, data )
{
   // this = CKFinderAPI
   var sFileName = this.getSelectedFile().name;
   document.getElementById( 'thumbnails' ).innerHTML +=
         '<div class="thumb">' +
            '<img src="' + fileUrl + '" />' +
            '<div class="caption">' +
               '<a href="' + data["fileUrl"] + '" target="_blank">' + sFileName + '</a> (' + data["fileSize"] + 'KB)' +
            '</div>' +
         '</div>';

   document.getElementById( 'preview' ).style.display = "";
   // It is not required to return any value.
   // When false is returned, CKFinder will not close automatically.
   return false;
}
   </script>
    
    <!-- TS1514865795: Neon - Responsive Admin Template created by Laborator -->
             <script type="text/javascript">
                jQuery(document).ready(function($) {
                    var $table1 = jQuery('#table-1');
                    // Initialize DataTable
                    $table1.DataTable({
                        "aLengthMenu": [
                            [10, 25, 50, -1],
                            [10, 25, 50, "All"]
                        ],
                        "bStateSave": true
                    });
                    // Initalize Select Dropdown after DataTables is created
                    $table1.closest('.dataTables_wrapper').find('select').select2({
                        minimumResultsForSearch: -1
                    });
                });
            </script>
            <script type="text/javascript">
                function showAjaxModal() {
                    jQuery('#modal-7').modal('show', {
                        backdrop: 'static'
                    });
                    jQuery.ajax({
                        url: "http://demo.neontheme.com/data/ajax-content.txt",
                        success: function(response) {
                            jQuery('#modal-7 .modal-body').html(response);
                        }
                    });
                }
            </script>
  
</head>

<body class="page-body page-fade" data-url="http://demo.neontheme.com">
    <!-- TS151486579512883: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates -->
    <div class="page-container">
        <!-- TS151486579512432: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates -->
        <div class="sidebar-menu">
            <div class="sidebar-menu-inner">
                <header class="logo-env">
                    <!-- logo -->
                    <div class="logo"> <a href="#"> <H2 style="color: #fff;margin-top: 2px;">CMC</H2> </a> </div>
                    <!-- logo collapse icon -->
                    <div class="sidebar-collapse">
                        <a href="#" class="sidebar-collapse-icon">
                            <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --><i class="entypo-menu"></i> </a>
                    </div>
                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation">
                            <!-- add class "with-animation" to support animation --><i class="entypo-menu"></i> </a>
                    </div>
                </header>

                



             

                <ul id="main-menu" class="main-menu">
 
                    <li> 
                        <a href="#">
                            <i class="fa fa-home"></i>
                            <span class="title">Dashboard</span>
                        </a>
                        
                    </li>


                    <li class="has-sub"> <a href="#"><i class="fas fa-home"></i><span class="title">List</span></a>
                                <ul>
                                     <li> <a href="<?php echo base_url(); ?>admin/viewallproduct"><i class="fas fa-sliders-h"></i><span class="title">View All List</span></a> </li>
                                    
                                </ul>
                     </li>

                      <li class="has-sub"> <a href="#"><i class="fas fa-headset"></i><span class="title">Enquiery</span></a>
                                <ul>
                                     <li> <a href="<?php echo base_url(); ?>admin/enquiery"><i class="fas fa-headset"></i><span class="title">View All Enquiery</span></a> </li>
                                    
                                </ul>
                     </li>




         
                    
                </ul>
            </div>
        </div>
        <div class="main-content">
            <!-- TS1514865795879: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates -->
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <ul class="user-info pull-left pull-none-xsm">
                        <!-- Profile Info -->
                        <li class="profile-info dropdown">
                           
                             <!-- <?php //echo ($this->session->userdata('Userprofileimg')); ?> -->
                            <!-- add class "pull-right" if you want to place this from right -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- "<?php //echo ($this->session->userdata('Userprofileimg')); ?>" -->
                             <!-- <img src= alt="" class="img-circle" width="44" /><!-- <?php// echo ($this->session->userdata('UsrUname')); ?> --></a> 
                            <ul class="dropdown-menu">
                                <!-- Reverse Caret -->
                                <li class="caret"></li>
                                <!-- Profile sub-links -->
                                
                               <!--  <li> <a for="<?php //echo ($this->session->userdata('Usrid')); ?>"  data-toggle="modal" data-target="#modal-6" class="btn_edituser" style="cursor: pointer;"> <i class="entypo-user"></i>Edit Profile</a> </li>
                                 <li> <a for="<?php //echo ($this->session->userdata('Usrid')); ?>"  data-toggle="modal" data-target="#modal-7" class="btn_userprofile" style="cursor: pointer;"> <i class="entypo-user"></i>Profile</a> </li>
                                -->
                            </ul>
                        </li>
                    </ul>
                    
                </div>
                <!-- Raw Links -->
                
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">
                    <ul class="list-inline links-list pull-right">
                         Welcome <b style="text-transform:capitalize;"> </b>
                         <?php 

                            // print_r($_SESSION); 
                           echo  $this->session->userdata('user_name');
                          ?>
                        <li> <a href="<?php echo base_url(); ?>Admin/logout">Log Out <i class="entypo-logout right"></i> </a> </li>
                         
                    </ul>
                </div>
            </div>
            <hr />
